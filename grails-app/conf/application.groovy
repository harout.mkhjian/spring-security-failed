
grails.plugin.springsecurity.filterChain.chainMap = [
		[pattern: '/assets/**', filters: 'none'],
		[pattern: '/**/js/**', filters: 'none'],
		[pattern: '/**/css/**', filters: 'none'],
		[pattern: '/**/images/**', filters: 'none'],
		[pattern: '/**/favicon.ico', filters: 'none'],
		[pattern: '/**', filters: 'JOINED_FILTERS']
]

development {
	useH2 = false
	initMockEntries = false
}


dataSource {
	pooled = true
	driverClassName = "com.mysql.cj.jdbc.Driver"
	dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}

// environment specific settings
environments {
	development {

		dataSource {
			username = "root"
			password = ""
			dbCreate = "update"
			url = "jdbc:mysql://localhost:3306/springsecurity?useUnicode=yes&characterEncoding=UTF-8"
			properties {
				initialSize = 5
				maxActive = 50
				minIdle = 5
				maxIdle = 25
				maxWait = 10000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
				validationQuery = "SELECT 1"
			}
		}


	}

	test {
		dataSource {
			username = "root"
			password = "harout"
			dbCreate = "update"
			url = "jdbc:mysql://localhost:3306/springsecurity?useUnicode=yes&characterEncoding=UTF-8&autoreconnect=true"
			pooled = true
			properties {
				initialSize = 5
				maxActive = 100
				maxIdle = 50
				minIdle = 10
				maxWait = 10000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
				validationQuery = "SELECT 1"
			}
		}
	}

	production {
		def envVar = System.env.VCAP_SERVICES
		// changes to deploy on Amazon AWS

		dataSource {
			username = "root"
			password = "harout"
			dbCreate = "update"
			url = "jdbc:mysql://localhost:3306/springsecurity?useUnicode=yes&characterEncoding=UTF-8&autoreconnect=true"
			pooled = true
			properties {
				initialSize = 5
				maxActive = 100
				maxIdle = 50
				minIdle = 10
				maxWait = 10000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
				validationQuery = "SELECT 1"
			}
		}
	}
}





// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.springSecurity.User_Gohar'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.springSecurity.UserRole_Gohar'
grails.plugin.springsecurity.authority.className = 'com.springSecurity.Role_Gohar'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

//grails.plugin.springsecurity.filterChain.chainMap = [
//	[pattern: '/assets/**',      filters: 'none'],
//	[pattern: '/**/js/**',       filters: 'none'],
//	[pattern: '/**/css/**',      filters: 'none'],
//	[pattern: '/**/images/**',   filters: 'none'],
//	[pattern: '/**/favicon.ico', filters: 'none'],
//	[pattern: '/**',             filters: 'JOINED_FILTERS']
//]

