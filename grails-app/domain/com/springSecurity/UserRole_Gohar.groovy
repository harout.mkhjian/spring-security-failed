package com.springSecurity

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.codehaus.groovy.util.HashCodeHelper
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@ToString(cache=true, includeNames=true, includePackage=false)
class UserRole_Gohar implements Serializable {

	private static final long serialVersionUID = 1

	User_Gohar user
	Role_Gohar role

	@Override
	boolean equals(other) {
		if (other instanceof UserRole_Gohar) {
			other.userId == user?.id && other.roleId == role?.id
		}
	}

    @Override
	int hashCode() {
	    int hashCode = HashCodeHelper.initHash()
        if (user) {
            hashCode = HashCodeHelper.updateHash(hashCode, user.id)
		}
		if (role) {
		    hashCode = HashCodeHelper.updateHash(hashCode, role.id)
		}
		hashCode
	}

	static UserRole_Gohar get(long userId, long roleId) {
		criteriaFor(userId, roleId).get()
	}

	static boolean exists(long userId, long roleId) {
		criteriaFor(userId, roleId).count()
	}

	private static DetachedCriteria criteriaFor(long userId, long roleId) {
		UserRole_Gohar.where {
			user == User_Gohar.load(userId) &&
			role == Role_Gohar.load(roleId)
		}
	}

	static UserRole_Gohar create(User_Gohar user, Role_Gohar role, boolean flush = false) {
		def instance = new UserRole_Gohar(user: user, role: role)
		instance.save(flush: flush)
		instance
	}

	static boolean remove(User_Gohar u, Role_Gohar r) {
		if (u != null && r != null) {
			UserRole_Gohar.where { user == u && role == r }.deleteAll()
		}
	}

	static int removeAll(User_Gohar u) {
		u == null ? 0 : UserRole_Gohar.where { user == u }.deleteAll() as int
	}

	static int removeAll(Role_Gohar r) {
		r == null ? 0 : UserRole_Gohar.where { role == r }.deleteAll() as int
	}

	static constraints = {
		role validator: { Role_Gohar r, UserRole_Gohar ur ->
			if (ur.user?.id) {
				UserRole_Gohar.withNewSession {
					if (UserRole_Gohar.exists(ur.user.id, r.id)) {
						return ['userRole.exists']
					}
				}
			}
		}
	}

	static mapping = {
		id composite: ['user', 'role']
		version false
	}
}
